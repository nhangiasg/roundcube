<?php declare(strict_types=1);

require '/var/www/html/webmail/vendor/autoload.php';
require '/var/www/html/webmail/program/lib/Roundcube/bootstrap.php';
require '/var/www/html/webmail/program/lib/Roundcube/rcube_text2html.php';
require '/var/www/html/webmail/program/lib/Roundcube/rcube_string_replacer.php';

// ini_set('display_errors', 1);
// ini_set('error_reporting', E_ALL);
// phpinfo();



/** @var PhpFuzzer\Config $config */
$config->setTarget(function(string $input) {
    if (strlen($input) >= 4 && $input[0] == 'z' && $input[3] == 'k') {
        throw new Error('Bug!');
    }
});

// $config->setTarget(function(string $input) {
//     $text = "[<h1>abc</h1>]http://def.com";
//     $t2h  = new rcube_text2html($text, false);
//     var_dump($t2h->get_html());
// });

?>