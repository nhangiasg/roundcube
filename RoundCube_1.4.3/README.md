RoundCube 1.4.3
=================


## Build 


0. Enable installer in `web/rootfs/var/www/html/webmail/config/config.inc.php`
1. Run ```docker compose up -d```
2. Create your first email address and an admin user by running ```docker compose run --rm web setup.sh```.

## Debug

1. Copy source `docker-compose cp web:/var/www/html/webmail ./web/webmail`
2. Use VSCode to debug


## Services

| Service                                    | Address                      |
| ------------------------------------------ | ---------------------------- |
| POP3 (starttls needed)                     | 127.0.0.1:110                |
| POP3S                                      | 127.0.0.1:995                |
| IMAP (starttls needed)                     | 127.0.0.1:143                |
| IMAPS                                      | 127.0.0.1:993                |
| SMTP                                       | 127.0.0.1:25                 |
| Mail Submission (starttls needed)          | 127.0.0.1:587                |
| Mail Submission (SSL, disabled by default) | 127.0.0.1:465                |
| Management Interface                       | http://127.0.0.1:81/manager/ |
| Webmail                                    | http://127.0.0.1:81/webmail/ |
| Rspamd Webinterface                        | http://127.0.0.1:81/rspamd/  |
